# basic_run.py
# provides code to perform a basic training run
# usage:
# python basic_run.py sa_id output_dir training_iterations use_cuda g_sa_id jitter seed learning_rate learning_momentum start_state_file

import load_test as lt
import st_test as st
import os
import sys

def configureModel(sm):
    sm.p_model.enable_w1_factor = 1 # N  (1 means enabled, 0 means disabled)
    sm.p_model.enable_w2_factor = 0 # G
    sm.p_model.enable_w4_factor = 0 # T
    sm.p_model.enable_w1 = 1 # N  (1 means enabled, 0 means disabled)
    sm.p_model.enable_w2 = 1 # G
    sm.p_model.enable_w4 = 1 # T
    sm.g_model.enable_mu = 1
    sm.g_model.enable_omega_normalisation = 0
    sm.st_model.enable_nu = 1
    sm.st_model.enable_phi_normalisation = 0
    sm.n_model.enable_n_scaling = 0
    sm.dt_model.enable_dt_scaling = 0

def parse_bool(str):
    if str in ['True','true','T','t','1']:
        return True
    elif str in ['False','false','F','f','0']:
        return False
    else:
        raise ValueError(f'Unable to parse string {str} as bool')
        
def main():
    cliParams = sys.argv[1:] # 0th entry is program name
    print("Basic Run: " + str(cliParams))

    # load cli params
    sa_ids = cliParams[0].split(',')
    output_dir = cliParams[1]
    training_iterations = int(cliParams[2])
    use_cuda = False if len(cliParams) < 4 else parse_bool(cliParams[3])
    g_sa_id = None if len(cliParams) < 5 else cliParams[4]
    if g_sa_id in sa_ids:
        g_sa_id = None
    jitter = 0.0 if len(cliParams) < 6 else float(cliParams[5])
    seed = 1 if len(cliParams) < 7 else int(cliParams[6])
    learning_rate = 0.00001 if len(cliParams) < 8 else float(cliParams[7])
    learning_momentum = 0.1 if len(cliParams) < 9 else float(cliParams[8])
    start_state_file = None if len(cliParams) < 10 else cliParams[9]
    start_epoch = 0 if len(cliParams) < 11 else int(cliParams[10])

    if start_state_file == "none":
        start_state_file = None
        start_epoch = 0

    sas = list(map(lambda sa_id: lt.load_sa_byname(sa_id), sa_ids))
    print("Loaded Study Areas " + str(sa_ids))
    
    g_sa = None if g_sa_id is None else lt.load_sa_byname(g_sa_id)
    if not g_sa is None:
        print("Loaded (G) Study Area " + g_sa.name)

    # init Torch
    st.init_torch(use_cuda, seed)

    # prepare data
    sads = list(map(lambda sa: st.StudyAreaData(sa), sas))
    g_sad = None if g_sa is None else st.StudyAreaData(g_sa)
    print("Prepared Study Area Data")

    # init model
    sm = st.StaticModel(sads[0])
    
    # enable and disable components
    configureModel(sm)
    print("Initialised Model")
    
    if not start_state_file is None:
        sm.load_state(start_state_file)
    else:
        sm.jitter(jitter)
    
    # train model
    st.train_many(sm, sads, g_sad, training_iterations, output_dir, 100, learning_rate, learning_momentum, start_epoch)
    print("Finished Training")

    # save parameters in various ways
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    sm.save_parameters(f'{output_dir}/')
    sm.save_real_parameters(f'{output_dir}/real')
    st.translate_real_params(f'{output_dir}/real')

    if len(sads) == 1:
        sad = sads[0]
        # produce final prediction
        (prob_prediction, _) = sm.predict(sad)
        
        # save actual and predicted
        lt.save_2d(sad.p_actual.cpu().numpy().T, f'{output_dir}/p_actual.csv')
        lt.save_2d(prob_prediction.detach().cpu().numpy().T, f'{output_dir}/prob_prediction.csv')

    print("Finished Everything")

if __name__ == "__main__":
    main()
