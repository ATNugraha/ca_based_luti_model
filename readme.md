# Model
 - The model and training code is in st_train.py
 - Data loading (and some export) code is in load_test.py

# Performing training
 - basic_run.py performs a training sequence with on the given study area with the given configuration (optimisation steps, learning rate, initial conditions (for informed runs) etc.)
 - Incremental output is pt files, which are pytorch state files.
 - Terminal output is many csvs, which should be human intelligable.
 - datadir.txt is the relative path to the data directory (i.e. model inputs and such).

# Input files
 - Per-study area files are suffixed with the study area id (e.g. Southampton has id 1)
 - Origin Destination Travel-time Matrices appear in the ODTM directory.
 - Lower Super Output Area data appears in the LSOA_raw directory, and describes the zones to which the zones are assigned.
 - Study area cell information is contained in the Cells directory. This includes information on location, terrain, transport accessibility, and land use. Land use values have been zeroed in the example due to data licencing.
 - N_profiling.csv contains the initial attration and repulsion coefficients