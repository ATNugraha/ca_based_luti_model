# st_test
# provides StudyAreaData, StaticModel, and code to train StaticModel on StudyAreaData

import load_test as lt
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import itertools
import math
import os

dtype = torch.FloatTensor 

def init_torch(usecuda, seed):
    if usecuda and torch.cuda.is_available():
        print("using cuda")
        device = torch.device('cuda')
        torch.cuda.device(device)
        
        dtype = torch.cuda.FloatTensor
    else:
        dtype = torch.FloatTensor
    
    torch.set_default_tensor_type(dtype)
    torch.manual_seed(seed)

def rescaled_soft_plus(x, scale):
    return ((x * scale).exp() + 1.0).log() / (scale.exp() + 1.0).log()

# use on numpy things
def np_inverse_softplus(x):
    if (x <= 0).any():
        raise "softplus violation"
    return np.log(np.exp(x) - 1)

def np_inverse_sigmoid(x):
    if (x <= 0).any():
        raise "sigmoid violation"
    if (x >= 1).any():
        raise "sigmoid violation"
    return 0 - np.log(1 / x - 1)

# scaled between 0 and 1
def scaling_f(x):
    sfactor = 0.01
    return ((1 + (-sfactor * (1 + (sfactor * x).exp()).log()).exp()).reciprocal()) * 2 - 1

def AR_Model_One(landuse_count, cell_count):
    ones = np.ones((landuse_count, landuse_count))
    gamma = np.random.randn(landuse_count, landuse_count)
    return AR_Model(landuse_count, cell_count, lt.AR_Params(ones, ones, gamma))

# these should be private statics in AR_Model
def fake_np_alpha(real_np_alpha):
    return np_inverse_softplus(real_np_alpha)

def fake_np_beta(real_np_beta):
    return np_inverse_softplus(real_np_beta)

def fake_np_gamma(real_np_gamma):
    return np_inverse_sigmoid(real_np_gamma / 4) # km

class AR_Model(nn.Module):
    def __init__(self, landuse_count, params_0):
        super(AR_Model, self).__init__()

        self.landuse_count = landuse_count
        self.alpha = nn.Parameter(torch.tensor(fake_np_alpha(params_0.alpha).astype('float32'), requires_grad = True))
        self.beta = nn.Parameter(torch.tensor(fake_np_beta(params_0.beta).astype('float32'), requires_grad = True))
        self.gamma = nn.Parameter(torch.tensor(fake_np_gamma(params_0.gamma).astype('float32'), requires_grad = True))

    def fill(self, other, v):
        self.alpha[:,v] = other.alpha[:,v]
        self.beta[:,v] = other.beta[:,v]
        self.gamma[:,v] = other.gamma[:,v]
        
    def set_params(self, params):
        self.alpha.data = torch.tensor(fake_np_alpha(params.alpha).astype('float32'))
        self.beta.data = torch.tensor(fake_np_beta(params.beta).astype('float32'))
        self.gamma.data = torch.tensor(fake_np_gamma(params.gamma).astype('float32'))
    
    def make_real_alpha(self):
        return F.softplus(self.alpha) # greater than zero

    def make_real_beta(self):
        return F.softplus(self.beta) # greater than zero NOTE: we used to scale this up by 1000, but it breaks the inverse computation

    def make_real_gamma(self):
        return torch.sigmoid(self.gamma) * 4 # squashed between 0 and 4, and scaled to m

    # d_trim_prefab is i, j
    # others (alpha, beta, gamma) are u, v
    def forward(self, d_trim, cell_count, local_cell_count):
        real_alpha = self.make_real_alpha()
        real_beta = self.make_real_beta()
        real_gamma = self.make_real_gamma()
        
        # first, fudge everything into 4-D tensors of form v, j, u, i
        # (the format that N_Model takes)
        v = self.landuse_count
        j = cell_count
        u = self.landuse_count
        i = local_cell_count
        
        d4 = d_trim.transpose(0, 1).reshape(1, j, 1, i)#.repeat(v, 1, u, 1)
        alpha4 = real_alpha.transpose(0, 1).reshape(v, 1, u, 1)#.repeat(1, j, 1, i)
        beta4 = real_beta.transpose(0, 1).reshape(v, 1, u, 1)#.repeat(1, j, 1, i)
        gamma4 = real_gamma.transpose(0, 1).reshape(v, 1, u, 1)#.repeat(1, j, 1, i)
        ones4 = 1 #torch.ones(v, j, u, i, requires_grad = False)
        
        uv_excluder = torch.eye(n = v, m = u, requires_grad = False)
        ij_excluder = torch.eye(n = 1, m = i, requires_grad = False) #commented out for u=v excluder
        uv_excluder4 = uv_excluder.reshape(v, 1, u, 1)
        ij_excluder4 = ij_excluder.reshape(1, 1, 1, i).repeat(1, j, 1, 1) #commented out for u=v excluder
        excluder4 = 1 - (uv_excluder4 * ij_excluder4) #commented out for u=v excluder

        # OK... now we just operate on them as normal
        result = alpha4 - alpha4 * ((ones4 + (-beta4 * (d4 - gamma4)).exp())).reciprocal()
        excluded = result * excluder4
        return excluded

    def params(self):
        return [self.alpha, self.beta, self.gamma]

def fake_np_lambda1(real_np_lambda1):
    return np_inverse_softplus(real_np_lambda1 - 0.001)

def fake_np_lambda2(real_np_lambda2):
    return np_inverse_softplus(real_np_lambda2 - 0.001)

class N_Model(nn.Module):
    def __init__(self, landuse_count):
        super(N_Model, self).__init__()

        self.landuse_count = landuse_count
        self.lambda1 = nn.Parameter(torch.tensor(fake_np_lambda1(np.ones((landuse_count, landuse_count)) * 1.0001).astype('float32'), requires_grad = True))
        self.lambda2 = nn.Parameter(torch.tensor(fake_np_lambda2(np.ones((landuse_count, landuse_count)) * 1.0001).astype('float32'), requires_grad = True))
        self.enable_n_scaling = 1

    def fill(self, other, v):
        self.lambda1[v,:] = other.lambda1[v,:]
        self.lambda2[v,:] = other.lambda2[v,:]
    
    def set_params(self, lambda1, lambda2):
        self.lambda1.data = torch.tensor(fake_np_lambda1(lambda1).astype('float32'))
        self.lambda2.data = torch.tensor(fake_np_lambda2(lambda2).astype('float32'))
    
    def make_real_lambda1(self):
        return F.softplus(self.lambda1) + 0.001 # greater than 0

    def make_real_lambda2(self):
        return F.softplus(self.lambda2) + 0.001 # greater than 0
    
    # a/r are v, j, u, i
    # z_trim is j, u, i
    def forward(self, a, r, z_trim, cell_count, local_cell_count):
        real_lambda1 = self.make_real_lambda1()
        real_lambda2 = self.make_real_lambda2()
        
        inner_dim = self.landuse_count * local_cell_count

        # first, reshape into v, j, ui
        v = self.landuse_count
        j = cell_count
        u = self.landuse_count
        i = local_cell_count
        ui = inner_dim

        a3 = a.reshape(v, j, ui)
        r3 = r.reshape(v, j, ui)
        z3 = z_trim.reshape(1, j, ui) # (already j, u, i)
        lambda1_3 = real_lambda1.reshape(v, 1, u, 1).repeat(1, 1, 1, i).reshape(v, 1, ui)
        lambda2_3 = real_lambda2.reshape(v, 1, u, 1).repeat(1, 1, 1, i).reshape(v, 1, ui)
        
        
        z_sign = z3.sign() #  we add this, so that when z is 0, we kill the expression
        z_fudge = 1 - z3.sign() # we add this, so that we don't compute gradients for 0^lambda2
        
        na = (a3 * z_sign * (z3 + z_fudge).pow(lambda1_3)).sum(2)
        nr = (r3 * z_sign * (z3 + z_fudge).pow(lambda2_3)).sum(2)

        nar = na - nr

        if self.enable_n_scaling:
            n_fudge = 0.01 # FUDGE: add a small number, so that we don't produce zeros
            n_fudge_scale = (1 - 2 * n_fudge) # pad it top and bottom
            n = scaling_f(nar) * n_fudge_scale + n_fudge
        else:
            n = F.softplus(nar) + 0.00001
        
        return n

    def params(self):
        return [self.lambda1, self.lambda2]

def fake_np_mu(real_np_mu):
    return np_inverse_sigmoid((real_np_mu - 0.001) / 0.999)

def fake_np_omega(real_np_omega):
    return np_inverse_softplus(real_np_omega)

class G_Model(nn.Module):
    def __init__(self, geographic_factor_count, landuse_count):
        super(G_Model, self).__init__()

        self.geographic_factor_count = geographic_factor_count
        self.landuse_count = landuse_count
        self.omega = nn.Parameter(torch.tensor(fake_np_omega(np.ones((geographic_factor_count, landuse_count)) * 1).astype('float32'), requires_grad = True))
        self.mu = nn.Parameter(torch.tensor(fake_np_mu(np.ones((geographic_factor_count, landuse_count)) * 0.4).astype('float32'), requires_grad = True))
        self.enable_mu = 1
        self.enable_omega_normalisation = 0

    def fill(self, other, v):
        self.omega[:,v] = other.omega[:,v]
        self.mu[:,v] = other.mu[:,v]
    
    def set_params(self, omega, mu):
        self.omega.data = torch.tensor(fake_np_omega(omega).astype('float32'))
        self.mu.data = torch.tensor(fake_np_mu(mu).astype('float32'))
    
    def make_real_omega(self):
        return F.softplus(self.omega) # greater than 0
    
    def make_real_mu(self):
        return F.sigmoid(self.mu) * 0.999 + 0.001 # greater than 0
    
    def forward(self, eta):
        real_omega = self.make_real_omega()
        real_mu = self.make_real_mu()
        
        v = self.landuse_count
        x = self.geographic_factor_count
        j = eta.shape[1]

        spread_factor = torch.ones(1).float() * 10.0

        # eta'_v,x,j = 1-(1/mu_v,x)*(1-eta_x,j)
        # G_v,j = prod_x(soft_plus(spread*eta'_x,j)^omega_x,v)
        # reshape into v, x, j
        eta3 = eta.reshape(1, x, j)
        omega3 = real_omega.transpose(0, 1).reshape(v, x, 1)
        if self.enable_mu:
            mu3 = real_mu.transpose(0, 1).reshape(v, x, 1)
        else:
            mu3 = torch.ones(1)
        if self.enable_omega_normalisation:
            omega_sum3 = real_omega.sum(0).reshape(v, 1, 1)
        else:
            omega_sum3 = torch.ones(1)

        eta_prime3 = 1 - mu3.reciprocal() * (1 - eta3)
        eta_rescaled3 = rescaled_soft_plus(eta_prime3, spread_factor) * 0.99 + 0.01 # fudge to avoid 0^x divergence
        g = eta_rescaled3.pow(omega3 / omega_sum3).prod(1)
        return g

    def params(self):
        return [self.omega, self.mu]
    
    def normalised_real_omega(self):
        real_omega = F.softplus(self.omega)
        return (real_omega / real_omega.sum(0))

def fake_np_nu(real_np_nu):
    return np_inverse_sigmoid((real_np_nu - 0.001) / 0.999)

def fake_np_phi(real_np_phi):
    return np_inverse_softplus(real_np_phi)

class ST_Model(nn.Module):
    def __init__(self, static_transport_factor_count, landuse_count):
        super(ST_Model, self).__init__()

        self.static_transport_factor_count = static_transport_factor_count
        self.landuse_count = landuse_count
        self.phi = nn.Parameter(torch.tensor(fake_np_phi(np.ones((static_transport_factor_count, landuse_count)) * 1).astype('float32'), requires_grad = True))
        self.nu = nn.Parameter(torch.tensor(fake_np_nu(np.ones((static_transport_factor_count, landuse_count)) * 0.4).astype('float32'), requires_grad = True))
        self.enable_nu = 1
        self.enable_phi_normalisation = 0

    def fill(self, other, v):
        self.phi[:,v] = other.phi[:,v]
        self.nu[:,v] = other.nu[:,v]
    
    def set_params(self, phi, nu):
        self.phi.data = torch.tensor(fake_np_phi(phi).astype('float32'))
        self.nu.data = torch.tensor(fake_np_nu(nu).astype('float32'))
    
    def make_real_phi(self):
        return F.softplus(self.phi) # greater than 0
    
    def make_real_nu(self):
        return F.sigmoid(self.nu) * 0.999 + 0.001 # greater than 0
    
    def forward(self, rho):
        real_phi = self.make_real_phi()
        real_nu = self.make_real_nu()
        
        v = self.landuse_count
        y = self.static_transport_factor_count
        j = rho.shape[1]

        spread_factor = torch.ones(1).float() * 10.0

        # rho'_v,y,j = 1-(1/nu_v,y)*(1-rho_y,j)
        # ST_v,j = prod_y(soft_plus(spread*rho'_y,j)^phi_y,v)
        # reshape into v, y, j
        rho3 = rho.reshape(1, y, j)
        phi3 = real_phi.transpose(0, 1).reshape(v, y, 1)
        if self.enable_nu:
            nu3 = real_nu.transpose(0, 1).reshape(v, y, 1)
        else:
            nu3 = torch.ones(1)
        if self.enable_phi_normalisation:
            phi_sum3 = real_phi.sum(0).reshape(v, 1, 1)
        else:
            phi_sum3 = torch.ones(1)

        rho_prime3 = 1 - nu3.reciprocal() * (1 - rho3)
        rho_rescaled3 = rescaled_soft_plus(rho_prime3, spread_factor) * 0.99 + 0.01 # fudge to avoid 0^x divergence
        st = rho_rescaled3.pow(phi3 / phi_sum3).prod(1)
        return st
        
    def params(self):
        return [self.phi, self.nu]
    
    def normalised_real_phi(self):
        real_phi = F.softplus(self.phi)
        return (real_phi / real_phi.sum(0))

class Dummy(object):
    def __init__(self):
        pass

def fake_np_delta(real_np_delta):
    return np_inverse_sigmoid((real_np_delta - 1) / 4)

def fake_np_kappa(real_np_kappa):
    return np_inverse_softplus(real_np_kappa - 0.0001)

class DT_Model(nn.Module):
    def __init__(self, landuse_count, mode_count):
        super(DT_Model, self).__init__()

        self.landuse_count = landuse_count # u, v
        self.mode_count = mode_count # m
        self.delta = nn.Parameter(torch.tensor(fake_np_delta(np.ones((landuse_count, landuse_count, mode_count)).astype('float32') * 1.5), requires_grad = True))
        self.kappa = nn.Parameter(torch.tensor(fake_np_kappa(np.ones((landuse_count, landuse_count, mode_count)).astype('float32')), requires_grad = True))
        self.enable_dt_scaling = 0

    def fill(self, other, v):
        self.delta[:,v,:] = other.delta[:,v,:]
        self.kappa[:,v,:] = other.kappa[:,v,:]
    
    def set_params(self, delta, kappa):
        self.delta.data = torch.tensor(fake_np_delta(delta).astype('float32'))
        self.kappa.data = torch.tensor(fake_np_kappa(kappa).astype('float32'))
    
    def make_real_delta(self):
        return torch.sigmoid(self.delta) * 4 + 1
        
    def make_real_kappa(self):
        return F.softplus(self.kappa) + 0.0001 

    def forward(self, odtm, hpmrs, zone_map, zone_count):
        #real_delta = F.softplus(self.delta) + 1
        real_delta = self.make_real_delta()
        real_kappa = self.make_real_kappa()

        # HPMRS_{u, ii}
        # ODTM_{m, ii, jj}
        # delta_{u, v, m}
        
        dummy_self = self#Dummy()
        
        # OK... output is dt_{v, jj}, so we need to stuff numbers into a {v, jj, *} and dot along that last dimension, where * is m*u*ii
        dummy_self.m = m = self.mode_count
        dummy_self.u = u = self.landuse_count
        dummy_self.v = v = u
        dummy_self.ii = ii = zone_count
        dummy_self.jj = jj = ii
        
        # summation over u, m, and ii
        dummy_self.muii = muii = m * u * ii

        # canonical 5 shall be v, jj, m, u, ii
        dummy_self.odtm5 = odtm5 = odtm.transpose(0, 2).transpose(1, 2).reshape(1, jj, m, 1, ii).repeat(v, 1, 1, u, 1)
        dummy_self.delta5 = delta5 = real_delta.transpose(0, 1).transpose(1, 2).reshape(v, 1, m, u, 1).repeat(1, jj, 1, 1, ii)
        dummy_self.hpmrs5 = hpmrs5 = hpmrs.reshape(1, 1, 1, u, ii).repeat(u, jj, m, 1, 1)
        dummy_self.kappa5 = kappa5 = real_kappa.transpose(0, 1).transpose(1, 2).reshape(v, 1, m, u, 1).repeat(1, jj, 1, 1, ii)

        # squash
        dummy_self.odtm3 = odtm3 = odtm5.reshape(v, jj, muii)
        dummy_self.delta3 = delta3 = delta5.reshape(v, jj, muii)
        dummy_self.hpmrs3 = hpmrs3 = hpmrs5.reshape(v, jj, muii)
        dummy_self.kappa3 = kappa3 = kappa5.reshape(v, jj, muii)
        dummy_self.ones = ones = torch.ones(muii)

        # compute
        dummy_self.denom3 = denom3 = odtm3 ** delta3
        dummy_self.numer3 = numer3 = hpmrs3 * kappa3
        dummy_self.quotient3 = quotient3 = numer3 / denom3
        dummy_self.dot = dot = quotient3 @ ones
        
        dummy_self.adjusted = adjusted = dot / (m * u)

        # scaling
        if self.enable_dt_scaling:
            dummy_self.scaled = scaled = scaling_f(adjusted)
        else:
            dummy_self.scaled = scaled = adjusted
            
        dt_byzone = scaled
        dummy_self.dt = dt = scaled @ zone_map
        return (dt, dt_byzone)
        
    def params(self):
        return [self.delta, self.kappa]
    
class DT_Prob_Model(nn.Module):
    def __init__(self):
        super(DT_Prob_Model, self).__init__()
        pass # (no parameters)

    def forward(self, dt_byzone, hpmrs_percell):
        dtp = dt_byzone
        # reciprocal(mean value for each land-usage) * mean study area land usage
        dtpbar = (dtp @ torch.ones(dtp.size()[1], 1)).reciprocal() * hpmrs_percell
        dtprob = dtp * dtpbar
        return dtprob

    def params(self):
        return []

def fake_np_tau1(real_np_tau1):
    return np_inverse_softplus(real_np_tau1)

def fake_np_tau2(real_np_tau2):
    return np_inverse_softplus(real_np_tau2)

class T_Model(nn.Module):
    def __init__(self, landuse_count):
        super(T_Model, self).__init__()
        self.landuse_count = landuse_count
        self.tau1 = nn.Parameter(torch.tensor(fake_np_tau1(np.ones(landuse_count)).astype('float32'), requires_grad = True))
        self.tau2 = nn.Parameter(torch.tensor(fake_np_tau2(np.ones(landuse_count)).astype('float32'), requires_grad = True))

    def fill(self, other, v):
        self.tau1[v] = other.tau1[v]
        self.tau2[v] = other.tau2[v]
    
    def set_params(self, tau1, tau2):
        self.tau1.data = torch.tensor(fake_np_tau1(tau1).astype('float32'))
        self.tau2.data = torch.tensor(fake_np_tau2(tau2).astype('float32'))
    
    def make_real_tau1(self):
        return F.softplus(self.tau1)

    def make_real_tau2(self):
        return F.softplus(self.tau2)

    def forward(self, st, dt):
        #return dt * st
        v = self.landuse_count
        
        real_tau1 = self.make_real_tau1()
        real_tau2 = self.make_real_tau2()
        
        tau1_2 = real_tau1.reshape(v, 1);
        tau2_2 = real_tau2.reshape(v, 1);
        
        normalisation_factor = torch.ones(1)#(tau1_2 + tau2_2).reciprocal()
        return st.pow(tau1_2 * normalisation_factor) * dt.pow(tau2_2 * normalisation_factor)

    def params(self):
        return [self.tau1, self.tau2]

def fake_np_w1(real_np_w1):
    return np_inverse_softplus(real_np_w1)

def fake_np_w2(real_np_w2):
    return np_inverse_softplus(real_np_w2)

def fake_np_w4(real_np_w4):
    return np_inverse_softplus(real_np_w4)

class P_Model(nn.Module):
    def __init__(self, landuse_count):
        super(P_Model, self).__init__()
        self.landuse_count = landuse_count
        self.w1 = nn.Parameter(torch.tensor(fake_np_w1(np.ones(landuse_count)).astype('float32'), requires_grad = True)) # for N
        self.w2 = nn.Parameter(torch.tensor(fake_np_w2(np.ones(landuse_count)).astype('float32'), requires_grad = True)) # for G
        self.w4 = nn.Parameter(torch.tensor(fake_np_w4(np.ones(landuse_count)).astype('float32'), requires_grad = True)) # for T
        self.enable_w1_factor = 1
        self.enable_w2_factor = 1
        self.enable_w4_factor = 1
        self.enable_w1 = 1
        self.enable_w2 = 1
        self.enable_w4 = 1
        
    def fill(self, other, v):
        self.w1[v] = other.w1[v]
        self.w2[v] = other.w2[v]
        self.w4[v] = other.w4[v]
    
    def set_params(self, w1, w2, w4):
        self.w1.data = torch.tensor(fake_np_w1(w1).astype('float32'))
        self.w2.data = torch.tensor(fake_np_w2(w2).astype('float32'))
        self.w4.data = torch.tensor(fake_np_w4(w4).astype('float32'))
    
    def make_real_w1(self):
        return F.softplus(self.w1)

    def make_real_w2(self):
        return F.softplus(self.w2)

    def make_real_w4(self):
        return F.softplus(self.w4)

    def forward(self, n, g, t):
        v = self.landuse_count
        
        real_w1 = self.enable_w1 * (self.make_real_w1() if self.enable_w1_factor else torch.ones(self.w1.shape)) # for N
        real_w2 = self.enable_w2 * (self.make_real_w2() if self.enable_w2_factor else torch.ones(self.w2.shape)) # for G
        real_w4 = self.enable_w4 * (self.make_real_w4() if self.enable_w4_factor else torch.ones(self.w4.shape)) # for T
        
        w1_2 = real_w1.reshape(v, 1)
        w2_2 = real_w2.reshape(v, 1)
        w4_2 = real_w4.reshape(v, 1)
        
        normalisation_factor = torch.ones(1)#(w1_2 + w2_2 + w4_2).reciprocal()
        return n.pow(w1_2 * normalisation_factor) * g.pow(w2_2 * normalisation_factor) * t.pow(w4_2 * normalisation_factor)

    def params(self):
        return [self.w1, self.w2, self.w4]

class Prob_Model(nn.Module):
    def __init__(self):
        super(Prob_Model, self).__init__()
        pass # (no parameters)

    def forward(self, p, hpmrs_percell):
        # TODO: move hpmrs_percell should be in its own box
        # reciprocal(total value for each land-usage) * total study area land usage
        pbar = (p @ torch.ones(p.size()[1], 1)).reciprocal() * hpmrs_percell
        prob = p * pbar
        return prob

    def params(self):
        return []

# these all give per-v errors

def ChiNotSquared(prob, prob_actual):
    diff = prob - prob_actual
    pre_sum = torch.abs(diff / prob)
    return sum(pre_sum.transpose(0, 1))

def ChiSquared(prob, prob_actual):
    diff = prob - prob_actual
    pre_sum = diff * diff / prob
    return sum(pre_sum.transpose(0, 1))

def SquaredError(prob, prob_actual):
    diff = prob - prob_actual
    pre_sum = diff * diff
    return sum(pre_sum.transpose(0, 1))

class StudyAreaData(object):

    def __init__(self, sa):
        self.sa = sa

        self.geographic_factor_count = 3
        self.static_transport_factor_count = 4
        self.landuse_count = sa.landuse_count()
        self.hpmrs_range = sa.hpmrs_range
        self.cell_count = sa.cell_count()
        self.mode_count = 2
        self.zone_count = sa.zone_count()

        # init test data
        #rho = torch.randn(static_transport_factor_count, cell_count).abs()
        self.max_local_distance = 4 # should be 4 in truth
        trim_stuff = sa.trim_stuff(self.max_local_distance)

        self.d_trim = torch.tensor(trim_stuff[0].astype('float32'))
        self.z_trim = torch.tensor(trim_stuff[1].astype('float32'))
        self.local_cell_count = self.d_trim.shape[0]
        
        self.rho = torch.tensor(sa.rho().astype('float32'))
        self.eta = torch.tensor(sa.eta().astype('float32'))
        self.odtm = torch.tensor(sa.odtm().astype('float32'))
        zeye = torch.eye(self.zone_count)
        self.odtm = self.odtm + zeye # make the diagonal all ones
        self.hpmrs = torch.tensor(sa.hpmrs_mat().astype('float32'))
        self.zone_map = torch.tensor(sa.zone_map().astype('float32'))
        #self.n_actual = torch.randn(landuse_count, cell_count).abs() # we can do better than this
        self.p_actual = torch.tensor(sa.z().astype('float32')).transpose(0, 1).contiguous()

        # total values for each landuse
        self.hpmrs_percell = (self.p_actual @ torch.ones(self.p_actual.size()[1], 1))

        self.sitedatas = [ self.d_trim, self.z_trim, self.rho, self.hpmrs ]

class StaticModel(nn.Module):

    def __init__(self, sad):
        super(StaticModel, self).__init__()
        
        # filter params
        (a_params, r_params) = lt.ar_params
        a_params = a_params.filter(sad.hpmrs_range)
        r_params = r_params.filter(sad.hpmrs_range)
        
        # init sub-models
        self.a_model = AR_Model(sad.landuse_count, a_params)
        self.r_model = AR_Model(sad.landuse_count, r_params)
        self.n_model = N_Model(sad.landuse_count)
        self.g_model = G_Model(sad.geographic_factor_count, sad.landuse_count)
        self.st_model = ST_Model(sad.static_transport_factor_count, sad.landuse_count)
        self.dt_model = DT_Model(sad.landuse_count, sad.mode_count)
        self.dt_prob_model = DT_Prob_Model()
        self.t_model = T_Model(sad.landuse_count)
        self.p_model = P_Model(sad.landuse_count)
        self.prob_model = Prob_Model()

        self.models = [self.a_model, self.r_model, self.n_model, self.g_model, self.st_model, self.dt_model, self.dt_prob_model, self.t_model, self.p_model, self.prob_model]

    def fill(self, other, v):
        self.a_model.fill(other.a_model, v)
        self.r_model.fill(other.r_model, v)
        self.n_model.fill(other.n_model, v)
        self.g_model.fill(other.g_model, v)
        self.st_model.fill(other.st_model, v)
        self.dt_model.fill(other.dt_model, v)
        self.t_model.fill(other.t_model, v)
        self.p_model.fill(other.p_model, v)

        # accumulate params
    def params(self):
        return itertools.chain.from_iterable([m.params() for m in self.models])

    # uses pytorch to do everything
    def save_state(self, filename):
        torch.save(self.state_dict(), filename)

    # uses pytorch to do everything
    def load_state(self, filename):
        self.load_state_dict(torch.load(filename))
        
    def save_parameters(self, prefix):
        torch.save(self.a_model.alpha, prefix + '_a_alpha.pt')
        torch.save(self.a_model.beta, prefix + '_a_beta.pt')
        torch.save(self.a_model.gamma, prefix + '_a_gamma.pt')
        
        torch.save(self.r_model.alpha, prefix + '_r_alpha.pt')
        torch.save(self.r_model.beta, prefix + '_r_beta.pt')
        torch.save(self.r_model.gamma, prefix + '_r_gamma.pt')
        
        torch.save(self.n_model.lambda1, prefix + '_lambda1.pt')
        torch.save(self.n_model.lambda2, prefix + '_lambda2.pt')

        torch.save(self.g_model.omega, prefix + '_omega.pt')
        torch.save(self.g_model.mu, prefix + 'mu.pt')

        torch.save(self.st_model.phi, prefix + '_phi.pt')
        torch.save(self.st_model.nu, prefix + '_nu.pt')

        torch.save(self.dt_model.delta, prefix + '_delta.pt')
        torch.save(self.dt_model.kappa, prefix + '_kappa.pt')
        
        torch.save(self.t_model.tau1, prefix + '_tau1.pt')
        torch.save(self.t_model.tau2, prefix + '_tau2.pt')

        torch.save(self.p_model.w1, prefix + '_w1.pt')
        torch.save(self.p_model.w2, prefix + '_w2.pt')
        torch.save(self.p_model.w4, prefix + '_w4.pt')
        
    def save_real_parameters(self, prefix):
        torch.save(self.a_model.make_real_alpha(), prefix + '_a_alpha.pt')
        torch.save(self.a_model.make_real_beta(), prefix + '_a_beta.pt')
        torch.save(self.a_model.make_real_gamma(), prefix + '_a_gamma.pt')
        
        torch.save(self.r_model.make_real_alpha(), prefix + '_r_alpha.pt')
        torch.save(self.r_model.make_real_beta(), prefix + '_r_beta.pt')
        torch.save(self.r_model.make_real_gamma(), prefix + '_r_gamma.pt')
        
        torch.save(self.n_model.make_real_lambda1(), prefix + '_lambda1.pt')
        torch.save(self.n_model.make_real_lambda2(), prefix + '_lambda2.pt')

        torch.save(self.g_model.make_real_omega(), prefix + '_omega.pt')
        torch.save(self.g_model.make_real_mu(), prefix + '_mu.pt')

        torch.save(self.st_model.make_real_phi(), prefix + '_phi.pt')
        torch.save(self.st_model.make_real_nu(), prefix + '_nu.pt')

        torch.save(self.dt_model.make_real_delta(), prefix + '_delta.pt')
        torch.save(self.dt_model.make_real_kappa(), prefix + '_kappa.pt')
        
        torch.save(self.t_model.make_real_tau1(), prefix + '_tau1.pt')
        torch.save(self.t_model.make_real_tau2(), prefix + '_tau2.pt')

        torch.save(self.p_model.make_real_w1(), prefix + '_w1.pt')
        torch.save(self.p_model.make_real_w2(), prefix + '_w2.pt')
        torch.save(self.p_model.make_real_w4(), prefix + '_w4.pt')

    def forward(self, sad):
        return self.predict(sad)[0]

    def predict(self, sad):
        a_prediction = self.a_model.forward(sad.d_trim, sad.cell_count, sad.local_cell_count)
        r_prediction = self.r_model.forward(sad.d_trim, sad.cell_count, sad.local_cell_count)
        n_prediction = self.n_model.forward(a_prediction, r_prediction, sad.z_trim, sad.cell_count, sad.local_cell_count)
        g_prediction = self.g_model.forward(sad.eta)
        st_prediction = self.st_model.forward(sad.rho)
        (dt_prediction, dt_prediction_by_zone) = self.dt_model.forward(sad.odtm, sad.hpmrs, sad.zone_map, sad.zone_count)
        t_prediction = self.t_model.forward(st_prediction, dt_prediction)
        p_prediction = self.p_model.forward(n_prediction, g_prediction, t_prediction)
        prob_prediction = self.prob_model.forward(p_prediction, sad.hpmrs_percell)

        return (prob_prediction, dt_prediction_by_zone)
    
    def jitter(self, amount):
        for p in self.params():
            p.data *= (amount * torch.randn(p.size()) + 1).data

def initOptimiser(sm, learning_rate, learning_momentum):
    # init optimizer
    opt = optim.SGD(sm.params(), lr=learning_rate, momentum=learning_momentum)
    #opt = optim.SGD(sm.params(), lr=0.00001, momentum=0.1)
    #opt = optim.Adam(sm.params())

    return opt
    
def load_pt_as_np(filename):
    t = torch.load(filename)
    t.requires_grad = False
    return t.cpu().numpy()
    
def load_scalar_pt_as_np(filename):
    t = torch.load(filename)
    return [t]

def translate_real_params(prefix):
    # ar
    a_alpha = load_pt_as_np(prefix + '_a_alpha.pt')
    a_beta = load_pt_as_np(prefix + '_a_beta.pt')
    a_gamma = load_pt_as_np(prefix + '_a_gamma.pt')
    
    r_alpha = load_pt_as_np(prefix + '_r_alpha.pt')
    r_beta = load_pt_as_np(prefix + '_r_beta.pt')
    r_gamma = load_pt_as_np(prefix + '_r_gamma.pt')
    
    a_params = lt.AR_Params(a_alpha, a_beta, a_gamma)
    r_params = lt.AR_Params(r_alpha, r_beta, r_gamma)
    lt.save_ar_params(a_params, r_params, prefix + '_ar.csv')
    
    # n
    lambda1 = load_pt_as_np(prefix + '_lambda1.pt')
    lambda2 = load_pt_as_np(prefix + '_lambda2.pt')
    
    lambdas = np.concatenate([lambda1, lambda2])
    lt.save_1d(lambdas, prefix + '_n.csv')
    
    # g
    omega = load_pt_as_np(prefix + '_omega.pt')
    mu = load_pt_as_np(prefix + '_mu.pt')
    lt.save_2d(omega, prefix + '_g_omega.csv')
    lt.save_2d(mu, prefix + '_g_mu.csv')
    
    # st
    phi = load_pt_as_np(prefix + '_phi.pt')
    nu = load_pt_as_np(prefix + '_nu.pt')
    lt.save_2d(phi, prefix + '_st_phi.csv')
    lt.save_2d(nu, prefix + '_st_nu.csv')
    
    # dt (no kappa, only delta)
    delta = load_pt_as_np(prefix + '_delta.pt')
    lt.save_3d(delta, prefix + '_dt.csv')
    
    kappa = load_pt_as_np(prefix + '_kappa.pt')
    lt.save_3d(kappa, prefix + '_kappa.csv')
    
    # t
    tau1 = load_pt_as_np(prefix + '_tau1.pt')
    tau2 = load_pt_as_np(prefix + '_tau2.pt')
    
    taus = np.concatenate([tau1, tau2])
    lt.save_1d(taus, prefix + '_t.csv')
    
    # p
    w1 = load_pt_as_np(prefix + '_w1.pt')
    w2 = load_pt_as_np(prefix + '_w2.pt')
    w4 = load_pt_as_np(prefix + '_w4.pt')
    
    ws = np.concatenate([w1, w2, w4])
    lt.save_1d(ws, prefix + '_p.csv')

def load_real_params(sm, prefix):
    landuse_count = sm.n_model.lambda1.shape[0]
    mode_count = sm.dt_model.delta.shape[2]
    
    # ar
    (a_params, r_params) = lt.load_ar_params(prefix + '_ar.csv', True, True)
    
    a_params.alpha[a_params.alpha < 0.0001] = 0.0001
    r_params.alpha[r_params.alpha < 0.0001] = 0.0001
    a_params.gamma[a_params.gamma < 0.0001] = 0.0001
    r_params.gamma[r_params.gamma < 0.0001] = 0.0001
    
    sm.a_model.set_params(a_params)
    sm.r_model.set_params(r_params)
    
    #n
    lambdas = lt.load_1d(prefix + '_n.csv')
    lambda1 = lambdas[:landuse_count,:]
    lambda2 = lambdas[landuse_count:,:]
    lambda1[lambda1 <= 0.001] = 0.0011
    lambda2[lambda2 <= 0.001] = 0.0011
    
    sm.n_model.set_params(lambda1, lambda2)

    # g
    omega = lt.load_2d(prefix + '_g_omega.csv')
    mu = lt.load_2d(prefix + '_g_mu.csv')
    mu[mu > 0.999] = 0.999
    
    sm.g_model.set_params(omega, mu)
    
    # st
    phi = lt.load_2d(prefix + '_st_phi.csv')
    nu = lt.load_2d(prefix + '_st_nu.csv')
    nu[nu > 0.999] = 0.999
    
    sm.st_model.set_params(phi, nu)
    
    # dt (no kappa, only delta)
    delta = lt.load_3d(prefix + '_dt.csv', mode_count)
    kappa = lt.load_3d(prefix + '_kappa.csv', mode_count)
    delta[delta >= 5] = 4.999
    delta[delta <= 1] = 1.001
    
    sm.dt_model.set_params(delta, kappa)
    
    # t
    taus = lt.load_1d(prefix + '_t.csv')
    tau1 = taus[:landuse_count]
    tau2 = taus[landuse_count:]
    
    sm.t_model.set_params(tau1, tau2)
    
    # p
    ws = lt.load_1d(prefix + '_P.csv')
    w1 = ws[:landuse_count]
    w2 = ws[landuse_count:landuse_count*2]
    w4 = ws[landuse_count*2:]
    
    sm.p_model.set_params(w1, w2, w4)

def kill_nans(t):
    nan_filter = (t == t)
    return t[nan_filter]

def dt_train(sm, sad, training_iterations = 1000):
    opt = initOptimiser(sm)

    # train
    for i in range(0, training_iterations):
        opt.zero_grad()

        (prob_prediction, dt_prediction_byzone) = sm.predict(sad)
        dt_prob = sm.dt_prob_model.forward(dt_prediction_byzone, sad.hpmrs_percell)
        
        # compute per-v loss
        loss_per_landuse = ChiSquared(dt_prob, sad.hpmrs)
        print (loss_per_landuse)

        # NaN-filter (this only makes sense because per-v models are independent)
        loss_per_landuse = kill_nans(loss_per_landuse)
        
        if loss_per_landuse.shape[0] == 0:
            break

        # compute final loss
        loss = loss_per_landuse.sum()
        
        if math.isnan(loss):
            break
        
        # back-prop
        loss.backward()
        
        opt.step()

def ensure_dir(dir):
    if not os.path.exists(dir):
        os.makedirs(dir)

def train(sm, sad, g_sad, training_iterations, save_dir, save_period, learning_rate, learning_momentum, start_epoch = 0):
    train_many(sm, [sad], g_sad, training_iterations, save_dir, save_period, learning_rate, learning_momentum, start_epoch)

def train_many(sm, sads, g_sad, training_iterations, save_dir, save_period, learning_rate, learning_momentum, start_epoch = 0):
    opt = initOptimiser(sm, learning_rate, learning_momentum)
    
    saving = save_period > 0
    if saving:
        ensure_dir(save_dir)
        
    loss_log = []
        
    # train
    for i in range(start_epoch, training_iterations):
        if saving and (i % save_period == 0):
            sm.save_state(os.path.join(save_dir, f"s{i}.pt"))
            
        opt.zero_grad()
        
        total_loss_total = 0.0
        loss_total = np.zeros((5))

        for sad in sads:
            (prob_prediction, dt_prediction_byzone) = sm.predict(sad)
    
            # compute per-v loss
            loss_per_landuse = ChiSquared(prob_prediction, sad.p_actual)
            
            # NaN-filter (this only makes sense because per-v models are independent)
            loss_per_landuse_no_nans = kill_nans(loss_per_landuse)
            
            # compute final loss
            loss = loss_per_landuse_no_nans.sum()
            
            if math.isnan(loss):
                break
            
            loss.backward()
            
            loss_total = loss_total + loss_per_landuse.detach().numpy()
            total_loss_total = total_loss_total + loss.detach().item()
    
        print (str(i) + " Total Loss: " + str(total_loss_total), flush = True) # hopefully flushing will mean we can see what is going on before it finishes    
        print (str(i) + " per-v Loss: " + str(loss_total))
        loss_log.append("T: " + str(list(loss_total)))
            
        opt.step()
        
        if (g_sad):
            (g_prob_prediction, g_dt_prediction_byzone) = sm.predict(g_sad)
            g_loss_per_landuse = ChiSquared(g_prob_prediction, g_sad.p_actual)
            print (str(i) + " per-v G_Loss: " + str(g_loss_per_landuse))
            loss_log.append("G: " + str(g_loss_per_landuse))
    
    if saving:
        sm.save_state(os.path.join(save_dir, "s_end.pt"))
        
    # save loss-log
    with open(os.path.join(save_dir, "loss_log.txt"), 'w') as f:
        for entry in loss_log:
            f.write("%s\n" % entry)
