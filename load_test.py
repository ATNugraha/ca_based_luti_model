# load_test.py
# provides code to load a Study_Area
# also provides mindless methods for opening csvs and xlsx, as well as saving numpy arrays as csvs

import os
import pandas as pd
import numpy as np
import scipy
import scipy.spatial
import bisect

data_dir_file = "datadir.txt"

def load_text(filename):
    with open(filename, 'r') as file:
        return file.read()

data_dir = load_text(data_dir_file)

def load_xls(filename):
    return pd.read_excel(os.path.join(data_dir, filename), sheet_name=0)

def load_hpmrs(filename, study_area):
    df = pd.read_excel(os.path.join(data_dir, filename), sheet_name=0)
    df = df[(df["StudyArea"] == study_area)]
    df = df.reset_index()
    df = df.drop("index", axis=1)
    return df

def load_mi(filename, study_area):
    df = pd.read_excel(os.path.join(data_dir, filename), sheet_name=0)
    df = df[(df["StudyArea"] == study_area)]
    df = df.reset_index()
    df = df.drop("index", axis=1)
    return df

def load_csv(filename):
    return pd.read_csv(os.path.join(data_dir, filename))

def load_csv_no_header(filename, working_dir = False):
    if working_dir:
        return pd.read_csv(filename, header=None)
    else:
        return pd.read_csv(os.path.join(data_dir, filename), header=None)

#
# STUDY AREAS
#

class StudyArea(object):
    def __init__(self, name, hpmrs, lsoa, odtm_car, odtm_public, cells):
        # study area friendly name
        self.name = name

        # per-zone land usage
        self.hpmrs = hpmrs

        # zone location, 'train', order, zone-name
        self.lsoa = lsoa

        # between cell car costs?
        self.odtm_car = odtm_car
        
        # between cell public transport costs?
        self.odtm_public = odtm_public

        # cell info: grid id, sa, easting, northing, zone_name, terrain, avg_slope, Dist_NavigableWaterfront, SRN_road, non_SRN_road, rail_links, dist_rail, housing, port, manufacture, retail, services
        self.cells = cells
        
        
        # configurable ranges
        self.hpmrs_range = [0, 1, 2, 3, 4] # default range, all land-uses
    
    # land use Z_{u,i}: only here it is Z_{i,u}: not going to change this now
    def z(self):
        excel_hmprs_range = [i + 13 for i in self.hpmrs_range]
        return self.cells.values[:,excel_hmprs_range]
    
    def landuse_count(self):
        return len(self.hpmrs_range)
    
    def cell_count(self):
        return len(self.cells)
    
    def zone_count(self):
        return len(self.lsoa)

    # maps zone_names to contiguous indexes
    def zone_indexer(self):
        
        # should index zones according to hpmrs
        
        zone_table = { }
        idx = 0
        k = self.zone_count()
        for i in range(0, k):
            zone_name = self.hpmrs["Zone_Name"][i]
            if not zone_name in zone_table:
                zone_table[zone_name] = idx
                idx += 1
        return zone_table
        
    # sorts hpmrs data by zone_id
    def hpmrs_zoned(self):
        return self.hpmrs

    # HPMRS_{u, ii}
    def hpmrs_mat(self):
        cell_to_zone = self.zone_indexer()
        cells = self.cells
        z = self.z()
        
        u = 5 # file landuse count
        k = self.zone_count()
        n = self.cell_count()
        
        mat = np.zeros([u, k])
        for i in range(0, n):
            zone_name = cells["Zone_Name"][i]
            zone_idx = cell_to_zone[zone_name]
            for v in range(0, u):
                mat[v, zone_idx] += z[i, v]
        
        return mat[self.hpmrs_range,:]

    # odtm table, ODTM_{m, ii, jj}
    def odtm(self):
        car = self.odtm_car.values[:,1:]
        public = self.odtm_public.values[:,1:]

        odtm = np.stack((car, public))
        return odtm
    
    # maps cells to zones
    def cell_to_zone_mapping(self):
        zone_table = self.zone_indexer()
        cell_table = { }
        n = self.cell_count()
        for i in range(0, n):
            cell_table[i] = zone_table[self.cells["Zone_Name"][i]]
        return cell_table

    # zone map matrix C_{j~,j}
    def zone_map(self):
        cz_map = self.cell_to_zone_mapping()
        m = self.zone_count()
        n = self.cell_count()
        c = np.zeros([m, n], dtype=float)
        for i in range(0, n):
            c[cz_map[i], i] = 1
        return c
    
    # computes the distance matrix D
    def d(self):
        locs = self.cells.values[:,3:5]
        
        d_meters = scipy.spatial.distance.squareform(scipy.spatial.distance.pdist(locs))
        d_km = d_meters / 1000
        return d_km

    # produces the trimmed stuff
    # returns (d_trim, z_trim)
    # dtrim is i, j
    # ztrim is j, u, i
    def trim_stuff(self, max_distance):
        def tval(elem):
            return elem[1]

        d = self.d()
        z = self.z()
        n = self.cell_count()
        landuse_count = self.landuse_count()

        # create a list of lists of the local cells
        local_lists = []
        for j in range(0, n):
            dj = list(enumerate(d[j], 0)) # (idx, dist)
            dj.sort(key = tval)
            dj_values = [e[1] for e in dj]
            end_idx = bisect.bisect_left(dj_values, max_distance + 0.00001)
            local_lists.append(dj[0:end_idx])

        max_count = max(len(l) for l in local_lists)
        
        z_trim = np.zeros([n, landuse_count, max_count])
        d_trim = np.zeros([max_count, n])

        for j in range(0, n):
            dj = local_lists[j]
            for i in range(0, len(dj)):
                idx = dj[i][0]
                val =  dj[i][1]
                # d_trim_indexes[i, j] = idx
                # d_trim_mapper[idx, j] = 1.0
                d_trim[i, j] = val
                for u in range(0, landuse_count):
                    z_trim[j, u, i] = z[idx, u]
                
        return (d_trim, z_trim)

    # computes the static accessibility matrix rho_{y, j}
    def rho(self):
        return self.cells.values[:,9:13].T
    
    # computes the geographic factors eta_{x, j}
    def eta(self):
        return self.cells.values[:,6:9].T

def scale_land_uses(df):
    return # disabled in favour of scaling alpha

def load_sa(name, sa_id):
    lsoa = load_csv(os.path.join("LSOAs_raw", f"LSOA_SA{sa_id}.csv"))
    odtm_car = load_csv(os.path.join("ODTMs", f"ODTM_CAR_SA{sa_id}.csv"))
    odtm_public = load_csv(os.path.join("ODTMs", f"ODTM_PUBLIC_SA{sa_id}.csv"))
    hpmrs = load_hpmrs(f"MI_LSOA_HPMRS_v3.xlsx", sa_id)
    scale_land_uses(hpmrs)
    cells = load_mi(os.path.join("Cells", f"sa{sa_id}.xlsx"), sa_id)
    scale_land_uses(cells)
    return StudyArea(name, hpmrs, lsoa, odtm_car, odtm_public, cells)

# aliases of special interest
def load_sa1():
    return load_sa("Southampton", 1)
def load_sa3():
    return load_sa("Dover", 3)
def load_sa6():
    return load_sa("Plymouth", 6)
def load_sa29():
    return load_sa("Falmouth", 29)
def load_sa54():
    return load_sa("Crash", 54)

def load_sa_byname(sa_id):
    if sa_id == "1":
        return load_sa1()
    elif sa_id == "3":
        return load_sa3()
    elif sa_id == "6":
        return load_sa6()
    elif sa_id == "29":
        return load_sa29()
    else:
        return load_sa(f"SA_{sa_id}", int(sa_id))

#
# PARAMS
#

class AR_Params(object):
    def __init__(self, alpha, beta, gamma):
        self.alpha = alpha
        self.beta = beta
        self.gamma = gamma
        
    def filter(self, hpmrs_range):
        return AR_Params(self.alpha[:,hpmrs_range], self.beta[:,hpmrs_range], self.gamma[:,hpmrs_range])

# returns (row_a, row_r)
def extract_n_params(arr, row):
    landuse_count = 5 # these are provided
    
    row_a = np.zeros([landuse_count, landuse_count])
    row_r = np.zeros([landuse_count, landuse_count])
    for i in range(0, landuse_count * landuse_count * 2):
        u = i // (landuse_count * 2)
        v = (i % (landuse_count * 2)) // 2
        if i % 2 == 0:
            row_a[u,v] = arr[row, i]
        else:
            row_r[u,v] = arr[row, i]

    return (row_a, row_r)

# returns (a, r)
def load_ar_params(filename, working_dir = False, no_scale = False):
    df = load_csv_no_header(filename, working_dir)
    (alpha_a, alpha_r) = extract_n_params(df.values, 0)
    (beta_a, beta_r) = extract_n_params(df.values, 1)
    (gamma_a, gamma_r) = extract_n_params(df.values, 2)
    
    # scale alpha
    if not no_scale:
        scaling = [200, 1, 6, 8, 5]
        scaling_mat = np.repeat(np.array([scaling]), 5, axis=0).T
        alpha_a = alpha_a / scaling_mat
        alpha_r = alpha_r / scaling_mat
    
    return AR_Params(alpha_a, beta_a, gamma_a), AR_Params(alpha_r, beta_r, gamma_r)

def save_ar_params(a_params, r_params, filename):
    a = np.stack([a_params.alpha, a_params.beta, a_params.gamma])
    r = np.stack([r_params.alpha, r_params.beta, r_params.gamma])
    d0 = a.shape[0]
    d1 = a.shape[1]
    d2 = a.shape[2]
    ar = np.stack([a, r], -1).reshape([d0, d1, d2*2])
    save_3d(ar, filename)

# save and load methods handle numpy arrays
def save_1d(arr, filename):
    np.savetxt(filename, arr, delimiter=',')
    
def save_2d(arr, filename):
    np.savetxt(filename, arr, delimiter=',')

# first dim is rows, other dimensions are squashed
def save_3d(arr, filename):
    d0 = arr.shape[0]
    d1 = arr.shape[1]
    d2 = arr.shape[2]
    flattened = arr.reshape([d0, d1 * d2])
    save_2d(flattened, filename)

def load_1d(filename):
    return np.loadtxt(filename, delimiter=',').astype('float32')

def load_2d(filename):
    return np.loadtxt(filename, delimiter=',').astype('float32')

def load_3d(filename, d2):
    s = load_2d(filename)
    d0 = s.shape[0]
    d1d2 = s.shape[1]
    d1 = d1d2 // d2
    return s.reshape([d0, d1, d2])

ar_params = load_ar_params("N_profiling.csv")

def main():
    pass

if __name__ == '__main__':
    main()
